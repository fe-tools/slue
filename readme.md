# slue

Similar to `gulp`, but use `read` instead of `src`, and `write` instead of `dest`.  Almost all of gulp's plugins are applicable for slue, except `gulp-sequence`, we use `slue-sequence` instead. Slue provide a sluepack.config.js file to bound modules and naturaly
can compile `react`、`vue` and `es6`. If you want to compile other type file, please use slue plugins.

`QQ GROUP: 738464890`

<a href="https://gitlab.com/fe-tools/vue-demo">slue + vue + vueRouter</a>

## sluepack.config.js
```javascript
var path = require('path');
module.exports = {
    // the map of package name and it's entry file
    entry: {
        app: './test/app.js'
    },

    // output of slue
    output: {
        // the root path of your output files 
        root: path.resolve(__dirname, './dev'),
        // path of js files(relative to root)
        jsPath: './',
        // other files path(relative to root)
        staticPath: './dev/static/',

        // example http://example.com/static/
        publicPath: './dev/static/'
    },

    // external global variables
    externals: {
        'react': 'React'
    },

    // when exclude return true, the required module will be ignore
    exclude: function(opts) {
        return opts.moduleId == 'xxx';
    },

    watch: true,

    // create sourceMap when sourceMap is true, default false
    sourceMap: true,

    // compile environment，development or production
    mode: 'production',

    // the type of output files.
    module: 'cmd',

    // alias make module require shorter 
    alias: {
        components: path.join(__dirname, './components')
    },

    // extension, used to compile the type of file which  
    plugins: [{
        exts: ['.js'], // files ext names
        use: [getOnePlugin, getOnePlugin] // plugins list
    }]
};
```

## __sluepack__

`__sluepack__` when use sluepack.config.js，slue self define a slue task named __sluepack__. __sluepack__ run after default task by default, however you can decide when to run __sluepack__ like bottom.

```javascript
slue.task('dev', ['__sluepack__'], function() {

});

const sequence = require('slue-sequence');
slue.task('dev', sequence(['clean', '__sluepack__', 'less']));
```

## API

`read` by provide one or a array list of glob, to read file by stream.

```javascript
const slue = require('slue');
const transformObj = require('slue-stream').transformObj;
slue.read('./test/a.js').pipe(transformObj(function(obj, env, cb) {
    cb(null, obj);
})).pipe(slue.write('./test/park'))
```

`write` write the stream piped files.

```javascript
const slue = require('slue');
const slueStream = require('slue-stream').transformObj;
slue.read('./test/a.js').pipe(transformObj(function(obj, env, cb) {
    cb(null, obj);
})).pipe(slue.write('./test/park'))
```

`watch` when some file change, to do something.

```javascript
const slue = require('slue');
let watcher = slue.watch(allFile);
watcher.on('change', function(filePath, stat) {
        
});
```

`task` define a task.

```javascript
const Bluebird = require('bluebird');
// define task a rely b.
slue.task('a', ['b'], function() {
    return new Bluebird(function(resolve) {
        console.log('task: c');
        resolve();
    });
});
```

`injectVars` inject slue.vars into file.

Inject vars into some file.
```javascript
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>TDNS平台</title>
</head>
<body>
    <script src="<%= slue.vars.app %>"></script>
</body>
</html>
```

Create vars and call slue.injectVars to inject vars into one file.
```javascript
const slue = require('slue');
const slueVars = require('slue-vars');
slue.task('less', function() {
    return slue.read(`./src/**/all.less`)
        .pipe($.less())
        .pipe(slueVars(function() {
            return {
                a: 'a',
                b: 'b'
            }
        }))
        .pipe($.postcss(processors))
        .pipe(slue.write(`./${env}`));
});
slue.task('useHtml', () => {
    return slue.injectVars('./src/index.html').pipe(slue.write('./dev'));
});
slue.task('dev', $.sequence(['less', 'useHtml']));
```
If your entry like this, you can get variable slue.vars.app
```javascript
    entry: {
        app: './src/main.js'
    }
```





## How to write a slue plugin
```javascript
const slueStream = require('slue-stream');
module.exports = function() {
    return slueStream.transformObj(function(file, env, cb) {
        // handle file
        cb(null, file);
    });
}
```

```javascript
const through2 = require('through2');
module.exports = function() {
    return through2.obj(function(file, env, cb) {
        // handle file
        cb(null, file);
    });
}
```

## Events

`read` by provide one or a array list of glob, to read file by stream.

```javascript
const slue = require('slue');
const transformObj = require('slue-stream').transformObj;
slue.on('sluepack.watcher.change', function() {
    console.log('sluepack.watcher.change');
});