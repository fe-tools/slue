function sequence(taskName, tasks, taskList = []) {
    let task = tasks[taskName];
    if (task) {
        taskList.push(taskName);
        if (task.dep) {
            task.dep.forEach(function(item) {
                sequence(item, tasks, taskList);
            });
        }
    }
}
module.exports = function(taskName, tasks) {
    let taskList = [];
    sequence(taskName, tasks, taskList);
    return taskList;
}