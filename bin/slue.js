#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const util = require('util');
const chalk = require('chalk');

const cwd = process.cwd();

let processArgvs = process.argv.slice(2);
if (processArgvs[0] === '-v') {
    let packageJSON = require('../package.json');
    console.log(`version: ${packageJSON.version}`);
    process.exit();
}

const modulePath = path.resolve(cwd, './node_modules/slue/index.js');
if (!fs.existsSync(modulePath)) {
    util.log(`${chalk.red('Local slue not found in')} ${chalk.redBright(cwd)}`);
    process.exit(1);
}

const configPath = path.resolve(cwd, './sluefile.js');
const sluePackConfigPath = path.resolve(cwd, './sluepack.config.js');
if (!fs.existsSync(configPath) && !fs.existsSync(sluePackConfigPath)) {
    util.log(`${chalk.red('Not sluefile.js or sluepack.config.js found')}`);
    process.exit(1);
}

if (fs.existsSync(configPath)) {
    require(configPath);
}

const slueInstance = require(modulePath);
if (fs.existsSync(sluePackConfigPath)) {
    let config = require(sluePackConfigPath);
    slueInstance.pack(config);
} else {
    slueInstance.run();
}