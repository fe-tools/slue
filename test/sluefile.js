const Bluebird = require('bluebird');
const slueStream = require('slue-stream');
const slue = require('../index');

let startTime = new Date().getTime();

// slue.task('c', function() {
//     return new Bluebird(function(resolve) {
//         console.log('task: c');
//         resolve();
//     });
// });

slue.task('c', ['d'], function() {
    return new Bluebird(function(resolve) {
        console.log('task: c');
        resolve();
    });
});

slue.task('d', ['e'], function() {
    console.log('task: d');
    return  stream = slueStream.transformObj();
});

slue.task('e', ['f'], function() {
    console.log('task: e');
});

slue.task('a', ['b'], function() {
    console.log('task: a');
});

slue.task('b', ['c'], function() {
    console.log('task: b');
});

slue.task('f', function() {
    console.log('task: f');
});

slue.task('f1', function() {
    console.log('task: f');
});

slue.task('f2', function() {
    console.log('task: f');
});

slue.task('f3', function() {
    console.log('task: f');
});

slue.task('f4', function() {
    console.log('task: f');
});

slue.task('f5', function() {
    console.log('task: f');
});

slue.task('f6', function() {
    console.log('task: f');
});

slue.task('f7', function() {
    console.log('task: f');
});

slue.task('f8', function() {
    console.log('task: f');
});

slue.task('f9', function() {
    console.log('task: f');
});

slue.task('f10', function() {
    console.log('task: f');
});

slue.task('f11', function() {
    console.log('task: f');
});

slue.task('f12', function() {
    console.log('task: f');
});

slue.task('f13', function() {
    console.log('task: f');
});

slue.task('f14', function() {
    console.log('task: f');
});

slue.task('f15', function() {
    console.log('task: f');
});

slue.task('f16', function() {
    console.log('task: f');
});

slue.task('f17', function() {
    console.log('task: f');
});

slue.task('f18', function() {
    console.log('task: f');
});

slue.task('f19', function() {
    console.log('task: f');
});

slue.task('f20', function() {
    console.log('task: f');
});

slue.task('f21', function() {
    console.log('task: f');
});

slue.task('f22', function() {
    console.log('task: f');
});

slue.task('f23', function() {
    console.log('task: ffffffff');
    let endTime = new Date().getTime();
    console.log(endTime - startTime);
});

slue.run();

//console.log(123456);