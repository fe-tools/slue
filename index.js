const util = require('util');
const Events = require('events');
const chalk = require('chalk');
const Bluebird = require('bluebird');
const slueFs = require('slue-fs');
const sluePack = require('slue-pack');
const slueStream = require('slue-stream');
const eos = require('end-of-stream');
const sequence = require('./lib/sequence');

function Slue() {
    this.tasks = {};
    Events.call(this);
}
util.inherits(Slue, Events);

Slue.prototype.read = slueFs.read;

Slue.prototype.write = slueFs.write;

Slue.prototype.watch = slueFs.watch;

Slue.prototype.run = function(taskList) {
    let me = this;
    if (!taskList) {
        let processArgvs = process.argv.slice(2);
        let taskName = processArgvs[0] || 'default';

        if (!me.tasks[taskName]) {
            console.log(chalk.red(`Task '${taskName}' is not in your sluefile`));
            process.exit(1);
        }

        taskList = sequence(taskName, me.tasks);
        taskList = taskList.reverse();

        if (me.tasks.__sluepack__ && taskName == 'default' && taskList.indexOf('__sluepack__') == -1) {
            taskList.unshift('__sluepack__');
        }
    }

    let promise = new Bluebird(function(resolve) {
        resolve();
    });
    taskList.forEach(function(name) {
        promise = promise.then(function(res) {
            return me.runOneTask(name);
        });
    });

    return promise;
}

Slue.prototype.runOneTask = function(taskName) {
    let me = this;
    if (!me.tasks[taskName]) {
        console.log(chalk.red(`Task '${taskName}' is not in your sluefile`));
        process.exit(1);
    }
    let task = me.tasks[taskName];
    return new Bluebird(function(resolve, reject) {
        if (task) {
            console.log(`Starting: ${chalk.green(task.name)}`);
            let _task = task.factory.call(me);
            if (_task) {
                if (typeof _task.then === 'function') {
                    _task.then(function() {
                        console.log(`Finished: ${chalk.green(task.name)}`);
                        resolve();
                    });
                } else if (typeof _task.pipe === 'function') {
                    eos(_task, {
                        error: true
                    }, function(err) {
                        console.log(`Finished: ${chalk.green(task.name)}`);
                        resolve();
                    });
                    // _task.on('end', function() {
                    //     console.log(`Finished: ${chalk.green(task.name)}`);
                    //     resolve();
                    // })
                    _task.pipe(slueStream.transformObj(function(obj, env, cb) {
                        cb(null, obj);
                    }));
                } else {
                    console.log(`Finished: ${chalk.green(task.name)}`);
                    resolve();
                }
            } else {
                console.log(`Finished: ${chalk.green(task.name)}`);
                resolve();
            }
        } else {
            resolve();
        }
    }).catch(function() {});
}

Slue.prototype.pack = function(config) {
    let me = this;
    me.task('__sluepack__', function() {
        return sluePack.bind(me)(config).then(function(compiler) {
            compiler.on('watcherchange', function() {
                me.emit('sluepack.watcher.change');
            });
        });
    });
    me.run();
}

Slue.prototype.task = function(name, dep, factory) {
    if (typeof dep === 'function') {
        factory = dep;
        dep = [];
    }

    if (!util.isArray(dep)) {
        console.log(chalk.red('deps must be array'));
        process.exit(1);
    }

    if (!util.isFunction(factory)) {
        console.log(chalk.red('factory must be function'));
        process.exit(1);
    }

    this.tasks[name] = {
        name: name,
        dep: dep,
        factory: factory
    };
}

function getPlugin(slue) {
    return slueStream.transformObj(function(file, env, cb) {
        let contents = file.contents.toString();
        contents = contents.replace(/<%=\s*slue\.vars\.(\w+)\s*%>/g, function(res, name) {
            if (slue.vars && slue.vars[name]) {
                return slue.vars[name];
            }
            return res;
        });
        file.contents = Buffer.from(contents);
        cb(null, file);
    })
}
Slue.prototype.injectVars = function(glogs) {
    return this.read(glogs).pipe(getPlugin(this));
}

module.exports = new Slue;