# slue changelog

## 2.4.8

### update slue-pack

## 2.4.9

### update slue-pack

## 2.4.10

### Performance improvement, update slue-pack

## 2.4.11

### Performance improvement, update slue-pack

## 2.4.12

### bug fix, update slue-pack

## 2.5.0

### support sourceMap, update slue-pack

## 2.5.1

### vue support sourceMap, update slue-pack

## 2.5.2

### update slue-pack

## 2.5.3

### update slue-pack

## 2.5.4

### update slue-pack

## 2.5.5

### update slue-pack

## 2.5.6

### update slue-pack

## 2.5.7

### slue + vue + vueRouter demo.

## 2.5.8

### update slue-pack.

## 2.5.9

### update dependence modules.

## 2.5.10

### entryFiles when more than one pack.

## 2.5.11

### sourcemap path add project folder.

## 2.5.12

### add sluepack.watcher.change event.

## 2.5.13

### sluePack bind the slue instance.

## 2.5.14

### support injectVars function.

## 2.5.17

### update slue-pack and slue-fs.